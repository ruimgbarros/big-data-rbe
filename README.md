# Template - Apresentação com package de R Xaringan

Template genérico para fazer apresentações com recurso ao package de R Xaringan

<!--- Slides podem ser vistos aqui [https://ruimgbarros.gitlab.io/formacao-dados/slides.html]( https://ruimgbarros.gitlab.io/formacao-dados )!---> 


Slides foram criados com [remark.js](http://remarkjs.com/) e o package de R [**xaringan**](https://github.com/yihui/xaringan)

O meu tema do xaringan (usando o  [xaringanthemer](https://pkg.garrickadenbuie.com/xaringanthemer/)):

```
mono_accent(
    base_color         = "#F48024",
    header_font_google = google_font("IBM Plex Sans", "700"),
    text_font_google   = google_font("IBM Plex Sans Condensed"),
    code_font_google   = google_font("IBM Plex Mono")
    )
```


